//
//  ViewController.swift
//  NiteKit
//
//  Created by tientt19 on 05/28/2023.
//  Copyright (c) 2023 tientt19. All rights reserved.
//

import UIKit
import Flutter
import FlutterPluginRegistrant

class ViewController: UIViewController {
    
    var flutterEngine: FlutterEngine?
    var flutterView: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.onConfigureFlutterChanel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onShowFlutter(_ sender: UIButton) {
        if let engine = self.flutterEngine {
            let flutterViewController = FlutterViewController(engine: engine, nibName: nil, bundle: nil)
            flutterViewController.modalPresentationStyle = .overCurrentContext
            flutterViewController.isViewOpaque = false
            self.flutterView = flutterViewController
            self.present(flutterViewController, animated: true)
        }
    }
}

extension ViewController {
    func onConfigureFlutterChanel() {
        self.flutterEngine = (UIApplication.shared.delegate as! AppDelegate).flutterEngine
        if let engine = self.flutterEngine {
            let channel = FlutterMethodChannel(name: "com.example.superapp/back", binaryMessenger: engine.binaryMessenger)
            channel.setMethodCallHandler { [weak self] (call, result) in
                if call.method == "onBack" {
                    self?.handleBackEvent(viewcontroller: self?.flutterView)
                }
            }
        }
    }
    
    func handleBackEvent(viewcontroller: UIViewController? = UIViewController()) {
        print("receive back event from flutter miniapp - dismiss")
        viewcontroller?.dismiss(animated: true)
    }
}

