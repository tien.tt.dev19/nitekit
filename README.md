# NiteKit

[![CI Status](https://img.shields.io/travis/tientt19/NiteKit.svg?style=flat)](https://travis-ci.org/tientt19/NiteKit)
[![Version](https://img.shields.io/cocoapods/v/NiteKit.svg?style=flat)](https://cocoapods.org/pods/NiteKit)
[![License](https://img.shields.io/cocoapods/l/NiteKit.svg?style=flat)](https://cocoapods.org/pods/NiteKit)
[![Platform](https://img.shields.io/cocoapods/p/NiteKit.svg?style=flat)](https://cocoapods.org/pods/NiteKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NiteKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NiteKit'
```

## Author

tientt19, tientt@elcom.com.vn

## License

NiteKit is available under the MIT license. See the LICENSE file for more info.
